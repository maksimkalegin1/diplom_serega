from django.db import models
from django.contrib.auth.models import User
from phonenumber_field.modelfields import PhoneNumberField


class Profile(models.Model):
    """
    Таблица профиля пользователя
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='Пользователь')
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    last_name = models.CharField(max_length=100, verbose_name='Фамилия')
    patronymic = models.CharField(max_length=100, verbose_name='Отчество')
    phone = PhoneNumberField(null=False, blank=False, unique=True, verbose_name='Телефон')
    birth_date = models.DateField(null=False, blank=False, verbose_name='Дата рождения')

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'

    def __str__(self):
        return self.first_name+' '+self.last_name

class Reservation(models.Model):
    """
    Таблица бронирования
    """
    reserve_from = models.DateTimeField(null=False, blank=False, verbose_name='От')
    reserve_to = models.DateTimeField(null=False, blank=False, verbose_name='До')
    people_amount = models.PositiveIntegerField(verbose_name='Количество людей')
    user = models.ForeignKey(Profile, on_delete=models.CASCADE, verbose_name='Пользователь')

    class Meta:
        verbose_name = 'Бронь'
        verbose_name_plural = 'Брони'

class Equipment(models.Model):
    """
    Таблица оборудования
    """
    name = models.CharField(max_length=200, null=False, blank=False, verbose_name='Название')
    article = models.CharField(max_length=100, verbose_name='Артикул')
    image = models.ImageField(null=True, blank=True, verbose_name='Фото')
    description = models.TextField(verbose_name='Описание')

    class Meta:
        verbose_name = 'Оборудование'
        verbose_name_plural = 'Оборудования'

    def __str__(self):
        return self.name

class Game(models.Model):
    """
    таблица игр
    """
    name = models.CharField(max_length=100, verbose_name='Название')
    description = models.TextField(verbose_name='Описание')
    release_date = models.DateField(verbose_name='Дата релиза')
    developer = models.CharField(max_length=100, verbose_name='Разработчик')
    photo = models.ImageField(verbose_name='Фото')
    category = models.ForeignKey('category', on_delete=models.CASCADE, null=True, blank=True, verbose_name='Категория')
    supported_equipment = models.ManyToManyField(Equipment, verbose_name='Оборудвание')

    class Meta:
        verbose_name = 'Игра'
        verbose_name_plural = 'Игры'

    def __str__(self):
        return self.name


class Category(models.Model):
    """
    Таблица категорий
    """
    name = models.CharField(max_length=100, verbose_name='название')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class Price(models.Model):
    """
    таблица цен
    """
    hour_price = models.PositiveIntegerField(verbose_name='цена в час')

    class Meta:
        verbose_name = 'Цена'
        verbose_name_plural = 'Цены'

    def __str__(self):
        return self.hour_price
