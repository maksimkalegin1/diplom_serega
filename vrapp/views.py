from django.shortcuts import render, redirect, HttpResponse
from .models import *
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from django.contrib import messages
import datetime


def index(request):
    today_date = datetime.date.today()
    if request.user.is_authenticated:
        reserves = Reservation.objects.filter(user=request.user.profile, reserve_to__gte=datetime.datetime.today())
        if len(reserves) >= 1:
            date_from = reserves[0].reserve_from.strftime('%m/%d %H:%M')
            messages.add_message(request, messages.ERROR, f"У вас есть бронь на {date_from}")
    return render(request, 'index.html', {'today_date': str(today_date)})


def games(request):
    categories = Category.objects.all()
    #games = {}
    #for category in categories:
    #    games[category.name] = Game.objects.filter(category=category)
    games = Game.objects.all()
    return render(request, 'games.html', {'games':games, 'categories': categories})


def devices(request):
    return render(request, 'devices.html')


def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            messages.add_message(request, messages.ERROR, "Неправильный логин или пароль")
            return redirect('/login/')
    else:
        return render(request, 'login.html')

def register_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        patronymic = request.POST.get('patronymic')
        birth_date = request.POST.get('birth_date')
        phone = request.POST.get('phone')
        try:
            user_object = User.objects.create_user(username=username, password=password)
        except Exception as exc:
            messages.add_message(request, messages.ERROR, "Пользователь с таким логином уже существует")
            return redirect('/signup/')
        profile_object = Profile.objects.create(phone=phone, user=user_object, patronymic=patronymic, last_name=last_name, first_name=first_name, birth_date=birth_date)
        profile_object.save()
        login(request, user_object)
        return redirect('/')
    else:
        return render(request, 'signup.html')

def logout_view(request):
    logout(request)
    return redirect('/')


def reservation(request):
    if request.user.is_authenticated:
        reserves = Reservation.objects.filter(user=request.user.profile, reserve_to__gte=datetime.datetime.today())
        if len(reserves) >= 1:
            messages.add_message(request, messages.ERROR, "Вы уже забронировали себе время")
            return redirect('/')
        date_from = request.POST.get('from')
        date_to = request.POST.get('to')
        guests = request.POST.get('guests')
        reserv = Reservation.objects.create(user=request.user.profile, reserve_from=date_from, reserve_to=date_to, people_amount=guests)
        reserv.save()
        return redirect('/')
    else:
        messages.add_message(request, messages.ERROR, "Вы не авторизовались")
        return redirect('/')
